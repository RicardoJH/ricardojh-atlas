﻿using UnityEngine;
using System.Collections;

public class EnemyPaddle : MonoBehaviour {

	public float speed = 8;
	Vector3 targetPos;
	Vector3 playerPos;
	GameObject ballObj;

	void Start() {

	}

	void FixedUpdate() {
		ballObj = GameObject.FindGameObjectWithTag ("Ball");
		if (ballObj != null) {
			targetPos = Vector3.Lerp (gameObject.transform.position, ballObj.transform.position, Time.deltaTime * speed);
			playerPos = new Vector3 (-11, Mathf.Clamp (targetPos.y, -3.3f, 3.3f), 0);
			gameObject.transform.position = new Vector3 (9, playerPos.y, 0);
		}
	}
}
