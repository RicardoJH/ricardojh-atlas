﻿using UnityEngine;
using System.Collections;

public class Paddle : MonoBehaviour {
	//2D Rigidbody is now a private variable named myRigidBody
	private Rigidbody2D myRigidBody;

	//paddleSpeed is adjustable from within Unity
	[SerializeField]
	private float paddleSpeed;

	public Vector3 playerPos;

	void FixedUpdate () {
		float yPos = gameObject.transform.position.y + Input.GetAxis("Vertical") * paddleSpeed;
		playerPos = new Vector3 (-11, Mathf.Clamp(yPos, -3.3f, 3.3f), 0);
		gameObject.transform.position = playerPos;
	}
}
