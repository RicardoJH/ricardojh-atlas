﻿using UnityEngine;
using System.Collections;

public class Score : MonoBehaviour {

	public TextMesh currentScore;
	public GameObject ballPrefab;
	public Transform paddleObj;

	GameObject ball;
	int score;

	void FixedUpdate() {
		ball = GameObject.FindGameObjectWithTag ("Ball");
		currentScore.text = "" + score;
	}

	void OnTriggerEnter(Collider other) {
		if (other.tag == "Ball"){
			score += 1;
			Destroy(ball);
			(Instantiate(ballPrefab, new Vector3(paddleObj.transform.position.x + 2, paddleObj.transform.position.y, 0), Quaternion.identity) as GameObject).transform.parent = paddleObj;
		}
	}
}
