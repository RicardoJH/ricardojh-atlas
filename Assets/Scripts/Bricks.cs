﻿using UnityEngine;
using System.Collections;

public class Bricks : MonoBehaviour {

	GameObject brick;

	void FixedUpdate() {
		brick = GameObject.FindGameObjectWithTag("Brick");
	}

	void OnTriggerEnter(Collider other) {
		if (other.tag == "Ball"){
			Destroy(brick);
		}
	}
}
